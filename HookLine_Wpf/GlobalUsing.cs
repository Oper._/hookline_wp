﻿global using MvvmHelpers;
global using MvvmHelpers.Commands;
global using HookLine_Wpf.Views;
global using HookLine_Wpf.ViewModels;
global using HookLine_Wpf.Models;
global using System.Windows.Input;
global using Microsoft.EntityFrameworkCore;