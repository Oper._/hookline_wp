﻿using System;
using System.Collections.Generic;

namespace HookLine_Wpf.Models
{
    public partial class Client
    {
        public Client()
        {
            OrderTovars = new HashSet<OrderTovar>();
        }

        public int Id { get; set; }
        public string? Email { get; set; }
        public string? Password { get; set; }
        public string? Name { get; set; }
        public string? PhoneNumber { get; set; }

        public virtual ICollection<OrderTovar> OrderTovars { get; set; }
    }
}
