﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace HookLine_Wpf.Models
{
    public partial class HookLineContext : DbContext
    {
        private static HookLineContext _context;
        public HookLineContext()
        {
        }

        public HookLineContext(DbContextOptions<HookLineContext> options)
            : base(options)
        {
        }

        public static HookLineContext GetContext()
        {
            if( _context == null )
                _context = new HookLineContext();
            return _context;
        }

        public virtual DbSet<Admin> Admins { get; set; } = null!;
        public virtual DbSet<Category> Categories { get; set; } = null!;
        public virtual DbSet<Client> Clients { get; set; } = null!;
        public virtual DbSet<Dostavka> Dostavkas { get; set; } = null!;
        public virtual DbSet<OrderItem> OrderItems { get; set; } = null!;
        public virtual DbSet<OrderTovar> OrderTovars { get; set; } = null!;
        public virtual DbSet<Tovar> Tovars { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=MIV\\SQLEXPRESS;Database=HookLine;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Admin>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Login).HasMaxLength(100);

                entity.Property(e => e.Password).HasMaxLength(100);
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.TypeCateg).HasMaxLength(50);
            });

            modelBuilder.Entity<Client>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Email).HasMaxLength(100);

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.Property(e => e.Password).HasMaxLength(100);

                entity.Property(e => e.PhoneNumber).HasMaxLength(100);
            });

            modelBuilder.Entity<Dostavka>(entity =>
            {
                entity.ToTable("Dostavka");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.TypeDostavki).HasMaxLength(50);
            });

            modelBuilder.Entity<OrderItem>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("OrderItem");

                entity.Property(e => e.IdOrder).HasColumnName("id_order");

                entity.Property(e => e.IdTovar).HasColumnName("id_tovar");

                entity.HasOne(d => d.IdOrderNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.IdOrder)
                    .HasConstraintName("FK_OrderItem_OrderTovar");

                entity.HasOne(d => d.IdTovarNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.IdTovar)
                    .HasConstraintName("FK_OrderItem_Tovar");
            });

            modelBuilder.Entity<OrderTovar>(entity =>
            {
                entity.ToTable("OrderTovar");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AdresPoluchately).HasMaxLength(150);

                entity.Property(e => e.IdClient).HasColumnName("id_client");

                entity.Property(e => e.IdDostavki).HasColumnName("id_dostavki");

                entity.HasOne(d => d.IdClientNavigation)
                    .WithMany(p => p.OrderTovars)
                    .HasForeignKey(d => d.IdClient)
                    .HasConstraintName("FK_OrderTovar_Clients");

                entity.HasOne(d => d.IdDostavkiNavigation)
                    .WithMany(p => p.OrderTovars)
                    .HasForeignKey(d => d.IdDostavki)
                    .HasConstraintName("FK_OrderTovar_Dostavka");
            });

            modelBuilder.Entity<Tovar>(entity =>
            {
                entity.ToTable("Tovar");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IdCategories).HasColumnName("id_categories");

                entity.Property(e => e.NameTovar).HasMaxLength(50);

                entity.HasOne(d => d.IdCategoriesNavigation)
                    .WithMany(p => p.Tovars)
                    .HasForeignKey(d => d.IdCategories)
                    .HasConstraintName("FK_Tovar_Categories");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
