﻿using System;
using System.Collections.Generic;

namespace HookLine_Wpf.Models
{
    public partial class OrderTovar
    {
        public int Id { get; set; }
        public int? IdClient { get; set; }
        public string? AdresPoluchately { get; set; }
        public int? IdDostavki { get; set; }
        public int? PriceDostavki { get; set; }

        public virtual Client? IdClientNavigation { get; set; }
        public virtual Dostavka? IdDostavkiNavigation { get; set; }
    }
}
