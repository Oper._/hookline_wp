﻿using System;
using System.Collections.Generic;

namespace HookLine_Wpf.Models
{
    public partial class Dostavka
    {
        public Dostavka()
        {
            OrderTovars = new HashSet<OrderTovar>();
        }

        public int Id { get; set; }
        public string? TypeDostavki { get; set; }

        public virtual ICollection<OrderTovar> OrderTovars { get; set; }
    }
}
