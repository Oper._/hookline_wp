﻿using System;
using System.Collections.Generic;

namespace HookLine_Wpf.Models
{
    public partial class Admin
    {
        public int Id { get; set; }
        public string? Login { get; set; }
        public string? Password { get; set; }
    }
}
