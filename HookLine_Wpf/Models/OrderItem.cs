﻿using System;
using System.Collections.Generic;

namespace HookLine_Wpf.Models
{
    public partial class OrderItem
    {
        public int? IdTovar { get; set; }
        public int? IdOrder { get; set; }
        public int? CountTovar { get; set; }

        public virtual OrderTovar? IdOrderNavigation { get; set; }
        public virtual Tovar? IdTovarNavigation { get; set; }
    }
}
