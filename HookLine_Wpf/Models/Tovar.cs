﻿using System;
using System.Collections.Generic;

namespace HookLine_Wpf.Models
{
    public partial class Tovar
    {
        public int Id { get; set; }
        public string? NameTovar { get; set; }
        public int? IdCategories { get; set; }
        public int? Price { get; set; }
        public byte[]? ImageTovar { get; set; }

        public virtual Category? IdCategoriesNavigation { get; set; }
    }
}
