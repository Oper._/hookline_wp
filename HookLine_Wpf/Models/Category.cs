﻿using System;
using System.Collections.Generic;

namespace HookLine_Wpf.Models
{
    public partial class Category
    {
        public Category()
        {
            Tovars = new HashSet<Tovar>();
        }

        public int Id { get; set; }
        public string? TypeCateg { get; set; }

        public virtual ICollection<Tovar> Tovars { get; set; }
    }
}
