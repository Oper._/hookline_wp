﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HookLine_Wpf.ViewModels
{
    public class TovarsPageViewModel
    {
        public List<Tovar> TovarsList { get; set; }

        public TovarsPageViewModel()
        {
            TovarsList = HookLineContext.GetContext().Tovars.Include(d => d.IdCategoriesNavigation).ToList();
        }
    }
}
