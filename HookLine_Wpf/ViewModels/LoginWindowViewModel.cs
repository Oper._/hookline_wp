﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace HookLine_Wpf.ViewModels
{
    public class LoginWindowViewModel
    {
        public string Login { get; set; }
        public string Password { private get; set; }
        public Command LoginCommand { get; set; }

        public LoginWindowViewModel()
        {
            LoginCommand = new Command(OnLoginClicked);
        }

        private async void OnLoginClicked(object obj)
        {
            Window wnd = obj as Window;
            Admin authUs = null;

            using (HookLineContext db = new HookLineContext())
            {
                authUs = await db.Admins.Where(b => b.Login == Login && b.Password == Password).FirstOrDefaultAsync();
            }

            if (authUs != null)
            {
                wnd.Close();
            }
            else
                MessageBox.Show("«Вы ввели неверный логин или пароль. Пожалуйста проверьте ещё раз введенные данные");
        }
    }
}
